var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var imgs = []

var boardSize
var maxSize = 0.9
var scaling
var ratio = 3 / 4
var initSize = 0.1
var numOfFrames = 54
var array = create2DArray(9, 6, 0, true, 0)

var randFrame
var randWidth
var randHeight
var randPos = {
  x: null,
  y: null
}

function preload() {
  for (var i = 0; i < numOfFrames; i++) {
    imgs.push(loadImage('img/breathless' + (i + 1) + '.jpeg'))
  }
}

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)

  scaling = (boardSize / 1440) * maxSize

  for (var i = 0; i < array.length; i++) {
    for (var j = 0; j < array[i].length; j++) {
      randFrame = Math.floor(Math.random() * imgs.length)
      randWidth = Math.floor(imgs[randFrame].width * Math.random() + 1)
      randHeight = Math.floor(imgs[randFrame].height * Math.random() + 1)
      randPos.x = Math.floor((imgs[randFrame].width - randWidth) * Math.random())
      randPos.y = Math.floor((imgs[randFrame].height - randWidth) * Math.random())
      array[i][j] = [imgs[randFrame].get(randPos.x, randPos.y, randWidth, randHeight), randFrame, randWidth, randHeight, randPos.x, randPos.y]
    }
  }

  array = array.flat()
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < array.length; i++) {
    push()
    translate(windowWidth * 0.5 - 1440 * 0.5 * scaling, windowHeight * 0.5 - 1080 * 0.5 * scaling)
    image(array[i][0], array[i][4] * scaling, array[i][5] * scaling, array[i][2] * scaling, array[i][3] * scaling)
    pop()
  }

  if (frameCount % 12 < 6) {
    array.unshift(array[array.length - 1])
    array.pop()
  } else {
    randFrame = Math.floor(Math.random() * imgs.length)
    randWidth = Math.floor(imgs[randFrame].width * Math.random() + 1)
    randHeight = Math.floor(imgs[randFrame].height * Math.random() + 1)
    randPos.x = Math.floor((imgs[randFrame].width - randWidth) * Math.random())
    randPos.y = Math.floor((imgs[randFrame].height - randWidth) * Math.random())
    array.push([imgs[randFrame].get(randPos.x, randPos.y, randWidth, randHeight), randFrame, randWidth, randHeight, randPos.x, randPos.y])
    if (array.length > numOfFrames) {
      array = array.splice(1)
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)

  scaling = (boardSize / 1440) * maxSize
}

function create2DArray(numRows, numCols, init, bool, max) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = Math.floor(Math.random() * max)
      }
    }
    array[i] = columns
  }
  return array
}
